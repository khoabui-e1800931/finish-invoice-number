package fi.vamk.studentid;

public class ReferenceValidator {
  public static void main(String[] args) {
    System.out.println("Hello World!");
    System.out.println(calculateRefNumber("123456"));
  }

  static char[] reverse(char a[], int length) {
    char[] b = new char[length];
    int j = length;
    for (int i = 0; i < length; i++) {
      b[j - 1] = a[i];
      j = j - 1;
    }

    return b;
  }

  public static String calculateRefNumber(String num) {
    try {
      char[] numChar = reverse(num.toCharArray(), num.toCharArray().length);

      String result = "";
      float resTmp = 0;
      int divider = 0;
      if (numChar.length <= 18) {
        char[] mul = "731731731731731731".toCharArray();

        for (int i = 0; i < numChar.length; i++) {
          resTmp += (mul[i] - 48) * (numChar[i] - 48);
        }

        divider = (int) Math.ceil(resTmp / 10) * 10;
        result = divider - (int) resTmp + "";
      }
      return num.concat(result);
    } catch (Exception e) {
      // TODO: handle exception
      return e.getMessage();
    }
  }

  public static String getActualCheckingNumber(String num) {
    char[] numChar = reverse(num.toCharArray(), num.toCharArray().length);
    String result = "";
    float resTmp = 0;
    int divider = 0;
    if (numChar.length <= 18) {
      char[] mul = "731731731731731731".toCharArray();

      for (int i = 0; i < numChar.length; i++) {
        resTmp += (mul[i] - 48) * (numChar[i] - 48);
      }

      divider = (int) Math.ceil(resTmp / 10) * 10;
      result = divider - (int) resTmp + "";
    }
    return result;
  }

}